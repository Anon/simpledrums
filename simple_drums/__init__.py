from st3m import logging
from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from st3m.ui import colours
from st3m.ui.view import ViewManager
from st3m.goose import Optional, Generator
from ctx import Context
import bl00mbox
import leds

class SimpleDrums(Application):
    log = logging.Log(__name__, level=logging.INFO)

    input: InputController
    channel: bl00mbox.Channel
    samples: list[bl00mbox.sampler] = []

    sample_names: list[str] = [
        "kick.wav",
        "snare.wav",
        "hihat.wav",
        "crash.wav",
        "open.wav",
        "close.wav"
    ]
    is_loading: bool = True

    def __init__(self, app_ctx: ApplicationContext):
        super().__init__(app_ctx)
        self.input = InputController()

        self.channel = bl00mbox.Channel("Simple Drums")

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)

        if self.is_loading:
            self._load_next_sample()
            return

        if self.input.captouch.petals[2].whole.pressed:
            self.samples[0].signals.trigger.start()
        if self.input.captouch.petals[4].whole.pressed:
            self.samples[1].signals.trigger.start()
        if self.input.captouch.petals[6].whole.pressed:
            self.samples[2].signals.trigger.start()
        if self.input.captouch.petals[8].whole.pressed:
            self.samples[3].signals.trigger.start()
        if self.input.captouch.petals[3].whole.pressed:
            self.samples[4].signals.trigger.start()
        if self.input.captouch.petals[7].whole.pressed:
            self.samples[5].signals.trigger.start()


    def draw(self, ctx: Context) -> None:
        ctx.rgb(*colours.BLACK)
        ctx.rectangle(
            -120.0,
            -120.0,
            240.0,
            240.0,
        ).fill()

        ctx.save()
        ctx.rgb(*colours.GO_GREEN)
        ctx.text_baseline = ctx.MIDDLE
        ctx.text_align = ctx.CENTER
        if self.is_loading:
            if len(self.samples) != len(self.sample_names):
                ctx.move_to(0, 0)
                ctx.font_size = 20
                ctx.font = "Camp Font 1"
                ctx.text(f"Loading sample {len(self.samples) + 1}/{len(self.sample_names)}")
                print(f"Loading sample {len(self.samples) + 1}/{len(self.sample_names)}")

            ctx.restore()
            return
        else:
            ctx.move_to(0, 20)
            ctx.font_size = 60
            ctx.font = "Material Icons"
            ctx.text("\ue050")
            ctx.restore()

        for i in range(5, 12):
            leds.set_rgb(i, 1.0, 0, 0)

        for i in range(13, 20):
            leds.set_rgb(i, 0.0, 0.0, 1.0)

        for i in range(21, 28):
            leds.set_rgb(i, 0.0, 1.0, 0.0)

        for i in range(29, 36):
            leds.set_rgb(i, 1.0, 1.0, 0.0)

        leds.update()


    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)

    def _load_next_sample(self) -> None:
        loaded = len(self.samples)
        if loaded >= len(self.sample_names):
            self.is_loading = False
            return

        i = loaded

        sample = self.channel.new(bl00mbox.patches.sampler, self.sample_names[i - 1])
        sample.signals.output = self.channel.mixer

        self.samples.append(sample)
